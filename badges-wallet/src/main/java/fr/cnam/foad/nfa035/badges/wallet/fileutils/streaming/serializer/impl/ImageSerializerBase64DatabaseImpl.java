package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;

/**
 *  Implémentation Base64 de sérialiseur d'image
 */
public class ImageSerializerBase64DatabaseImpl extends AbstractStreamingImageSerializer <File, ImageFrameMedia> {

    /**
     *
     * @param source
     * @return
     * @throws FileNotFoundException
     */

    @Override
    public  InputStream getSourceInputStream(File source) throws FileNotFoundException {
        return new FileInputStream(source);
    }

    /**
     *
     * @param media
     * @return
     * @throws IOException
     */

    @Override
    public  OutputStream  getSerializingStream(ImageFrameMedia media) throws IOException {
        return new Base64OutputStream((OutputStream)media);
    }
    @Override
    public final void serialize(File source, ImageFrameMedia media) throws IOException {
        long numberOfLines = Files.lines(((File)media.getChannel()).toPath()).count();
        long size = Files.size(source.toPath());
        try(OutputStream os = media.getEncodedImageOutput()) {
            Writer writer = new PrintWriter(os);


            writer.write(getSerializingStream(media).toString());
            writer.flush();
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                writer.write("\n");
            }
            writer.flush();
        }
    }



}

