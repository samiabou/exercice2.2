package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer<ResumableImageFrameMedia> {

    private OutputStream sourceOutputStream;

    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     */
    public ImageDeserializerBase64DatabaseImpl(OutputStream sourceOutputStream) {
        this.setSourceOutputStream(sourceOutputStream);
    }

    /**
     * {@inheritDoc}
     * @param media
     * @throws IOException
     */

    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {

        BufferedReader b = media.getEncodedImageReader(true);
        String[] data = b.readLine().split(";");

        try (OutputStream os = getSourceOutputStream()){
            getDeserializingStream(data[2]).transferTo(os);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public  OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     *
     * @param os
     */

    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.sourceOutputStream = os;

    }

    /**
     *
     * @param data
     * @return
     * @throws IOException
     */

    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
}



