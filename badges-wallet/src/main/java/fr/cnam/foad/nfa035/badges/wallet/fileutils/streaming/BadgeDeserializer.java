package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface définit le comportement d'un objet permet à la désérialisation d'une image.
 *@param <M>
 */

public interface BadgeDeserializer<M extends ImageFrameMedia> {

    /**
     * Méthode de désérialisation,qui transfert simple du flux de lecture vers un flux d'écriture
     * @param media
     * @throws IOException
     */


    void deserialize(M media) throws IOException;

    /** Utile pour récupérer un flux d'écriture
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * Utile pour modifier le flux de sortie
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);
}