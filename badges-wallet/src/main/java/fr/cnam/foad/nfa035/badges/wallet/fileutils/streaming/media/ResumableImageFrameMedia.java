package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Interface spécifié le comportementant du media de sérialisation
 */

public interface ResumableImageFrameMedia <T> extends ImageFrameMedia <T>{
    /**
     *Permet de la récupération du flux d'entrée, si on veut reprendre la lecture depuis le dernier point
     * @param resume
     * @return
     * @throws IOException
     */
     BufferedReader getEncodedImageReader(boolean resume) throws IOException;
}
